

let notclosed = document.getElementById('notclosed');
let assigntome = document.getElementById('assigntome');
let research = document.getElementById('research')
notclosed.addEventListener('change',action)
assigntome.addEventListener('change',action);
research.addEventListener('input', action)

function action(event){
  let httpRequest= new XMLHttpRequest();

  let eltcoche;
  if (assigntome.checked && notclosed.checked) {
    eltcoche='lesdeux';
    httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ eltcoche);
    
  }
  else if (notclosed.checked) {
    eltcoche='closed';
    httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ eltcoche);

  }
  else if (assigntome.checked) {
    eltcoche='assign';
    httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ eltcoche);
  }
  else{
    eltcoche='default'
    httpRequest.open('GET', 'bug/Engineer/search/'+ research.value +'&'+ eltcoche);
  }
  httpRequest.send();
  httpRequest.onreadystatechange = () =>{
    if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
        let parsedData = JSON.parse(httpRequest.responseText);
        let tableau='';
        let Public_Path='http://localhost:8000/ticketing/application-de-gestion-d-incidents/public/bug/show/';
        if (parsedData.reponse.length==0) {
            tableau = '<td colspan="7" style="text-align:center;vertical-align: middle;"> Aucun résultat</td>';
        }
        else{
          //console.log(parsedData.reponse)
            for (let i = 0; i < parsedData.reponse.length; i++) {
                let engineer;
                
                let closeddate;
                if (parsedData.reponse[i].engineer != null) {
                    engineer=parsedData.reponse[i].engineer
                }
                else{
                    engineer='<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'"  id="assigne_bug" onclick="assigner(event)">Assigner</button>'
                }

                if(parsedData.reponse[i].closedAt) {
                    closeddate = parsedData.reponse[i].closedAt.date.split(' ')[0];
                } 
                else if (parsedData.reponse[i].engineer != null /* && id engineer == id user connecté */) {
                  closeddate = '<button class="waves-effect waves-light btn-small" value="'+parsedData.reponse[i].id+'"  id="cloture_bug" onclick="cloturer(event)">Clôturer</button>';
                } 
                else {
                  closeddate ='En cours';
                };
                tableau=tableau+`<tr>
                <td>${parsedData.reponse[i].id}</td>
                <td>${parsedData.reponse[i].title}</td>
                <td>${parsedData.reponse[i].createdAt.date.split(' ')[0]}</td>
                <td>${parsedData.reponse[i].recorder}</td>
                <td><a class="waves-effect waves-light btn-small" href="${Public_Path+parsedData.reponse[i].id}"><i class="material-icons left">subject</i>afficher</a></td>
                <td>${engineer}</td>
                <td>${closeddate}</td>
                </tr>`;
                
            }
        }
        document.getElementById("donnees").innerHTML= tableau;
    }
  }
}

let boutonsAssigner = document.querySelectorAll('#assigne_bug');

boutonsAssigner.forEach(
  (element) => { 
    element.addEventListener('click', assigner);
  }
);


function ajoutEventClickCloturer() {
  let buttonCloturer = document.querySelectorAll('#cloture_bug');
  buttonCloturer.forEach(
    (element) => { 
      element.addEventListener('click', cloturer);
    }
  );
}

function assigner(event){
  //console.log(event.target.value);

  let httpRequest= new XMLHttpRequest();
  httpRequest.open('GET', 'bug/Engineer/assignerajax/'+ event.target.value);

  httpRequest.send();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
        let parsedData = JSON.parse(httpRequest.responseText);
        if(parsedData.statusOk == true) {
          event.target.parentElement.nextElementSibling.innerHTML = '<button class="waves-effect waves-light btn-small" value="'+event.target.value+'"  id="cloture_bug">Clôturer</button>';
          ajoutEventClickCloturer();
          event.target.parentElement.innerHTML = parsedData.engineer;
        } else {
          alert('Assignation impossible.')
        }
    }
  }
}

ajoutEventClickCloturer();

function cloturer(event) {
  //console.log(event.target.value);

  let httpRequest= new XMLHttpRequest();
  httpRequest.open('GET', 'bug/Engineer/cloturerajax/'+ event.target.value);

  httpRequest.send();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == XMLHttpRequest.DONE && httpRequest.status == 200) {
        let parsedData = JSON.parse(httpRequest.responseText);
        if(parsedData.statusOk == true) {
          event.target.parentElement.innerHTML = parsedData.closedAt.split(' ')[0 ];
        } else {
          alert('Impossible de clôturer.')
        }
    }
  }
}