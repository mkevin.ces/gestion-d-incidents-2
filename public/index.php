<?php

require('../src/init.php');

//permet d'exploiter les variables sessions
//ex : ($_SESSION["utilisateur"] -> recuperation de l'utilisateur connecté partout dans l'appli)
session_start();

use BugApp\Controllers\bugController;
use BugApp\Controllers\UserController;

switch(true) {

    case preg_match('#^bug/show/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->show($id);

        break;
    
    case preg_match('#^bug((\?)|$)#', $uri):

        $controller = new bugController();

        return $controller->index();

        break;

    case preg_match('#^bug/Engineer/search/(\w*)&(\w+)$#', $uri, $matches):

        $string = $matches[1];
        
        $id_filter = $matches[2];
            
        $controller = new bugController();
                
        return $controller->search_ajax($string,$id_filter);
                
        break;

    case preg_match('#^bug/Engineer/assignerajax/(\d+)$#', $uri, $matches):

        $id_bug = $matches[1];
            
        $controller = new bugController();
                
        return $controller->assigner_ajax($id_bug);
                
        break;

    case preg_match('#^bug/Engineer/cloturerajax/(\d+)$#', $uri, $matches):

        $id_bug = $matches[1];
            
        $controller = new bugController();
                
        return $controller->cloturer_ajax($id_bug);
                
        break;
    
    case ($uri == 'bug/add'):

        $controller = new bugController();
        
        return $controller->add();
        
        break;
   
    case ($uri == 'login'):
        
        $controller = new UserController();

        return $controller->login();

        break;

    case ($uri == 'logout'):
        
            $controller = new UserController();
    
            return $controller->logout();
    
            break;

    default:
    
    http_response_code(404);
    
    echo "<h1>Error</h1>";
   
}