<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue
        // Attribution du chemin en fonction du type d'utilisateur contenu dans la variable session
        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        $manager = new BugManager();
        $user = $_SESSION['user'];
        $type_user = $_SESSION['type'];
        switch($type_user){
            case 'engineer':
                $bugs = $manager->findAll();
                break;

            case 'recorder':
                $bugs = $manager->findAllRecorder($user->getId());
                break;

            default:
                die('You should not be there');
        }


        // TODO: liste des incidents
        // Attribution du chemin en fonction du type d'utilisateur contenu dans la variable session
        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/list', ['bugs' => $bugs, 'connectedUser' => $user]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident
        // TODO: ajout d'incident (GET et POST)
        //var_dump($_POST["textarea1"]);
        $manager = new BugManager();
        if(isset($_POST['action'])) {
            $bug = new Bug();
            $bug->setTitle($_POST["nom"]);
            $bug->setCreatedAt($_POST["date"].' 00:00:00');
            $bug->setDescription($_POST["desc"]);
            $manager->add($bug,$_SESSION['user']);         
            header('Location: http://localhost:8000/bug');
        }
        else{

        $content = $this->render('src/Views/Bug/Recorder/add', []);

        return $this->sendHttpResponse($content, 200);
        };
    }

    public function search_ajax($string,$id_filter)
    {
        $manager = new BugManager();
        $user = $_SESSION['user'];
        if ($id_filter=='assign') {
            $bugs=$manager->findAllEngineerAssigntome($user->getId(),$string);            
        }
        else if ($id_filter=='closed') {
            $bugs=$manager->findAllEngineerNotClosed($string);
        }
        else if ($id_filter=='lesdeux') {
            $bugs=$manager->findAllEngineerLesdeux($user->getId(),$string);
        }
        else if ($id_filter=='default') {
            $bugs=$manager->findAllsearch($string);

        }
        echo json_encode(["reponse"=>$bugs]);
            return;
    }

    public function assigner_ajax($id_bug)
    {
        $manager = new BugManager();
        $user = $_SESSION['user'];

        $bug = $manager->find($id_bug);

        if($bug->getEngineer() != null || $bug->getClosedAt() != null) {
            echo json_encode(["statusOk"=> false]);
            return;
        }

        $manager->updateAssigner($bug, $user);

        echo json_encode(["statusOk"=> true, "engineer" => $user->getNom()]);
        return;

    }

    public function cloturer_ajax($id_bug)
    {
        $manager = new BugManager();
        $user = $_SESSION['user'];

        $bug = $manager->find($id_bug);

        //Si le bug est pas assigné, ou le bug est déjà clôturé, ou le bug est assigné à un autre ingénieur
        //alors on ne fait pas la mise à jour de clôture
        if($bug->getEngineer() == null || $bug->getClosedAt() != null || $bug->getEngineer()->getId() != $user->getId()) {
            echo json_encode(["statusOk"=> false]);
            return;
        }

        $dateCloture = $manager->updateCloture($bug);

        echo json_encode(["statusOk"=> true, "closedAt" => $dateCloture]);
        return;

    }

    

}
