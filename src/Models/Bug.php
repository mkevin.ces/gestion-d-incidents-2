<?php

namespace BugApp\Models;

class Bug implements \JsonSerializable {
    
    public $id;
    public $title;
    public $description;
    public $createdAt;
    public $closed;
    private $recorder;
    private $engineer;

    function __construct() {
        
    }
   

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $createdAt);
        
        $this->createdAt = $date;

        return $this;
    }

    /**
     * Get the value of closed
     */ 
    public function getClosedAt()
    {
        return $this->closed;
    }

    /**
     * Set the value of closed
     *
     * @return  self
     */ 
    public function setClosedAt($closedAt)
    {
        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $closedAt);
        
        $this->closed = $date;

        return $this;
    }

     /**
     * Get the value of recorder
     */ 
    public function getRecorder()
    {
        return $this->recorder;
    }

    /**
     * Set the value of recorder
     */ 
    public function setRecorder($recorder)
    {
        $this->recorder = $recorder;
    }

    /**
     * Get the value of engineer
     */ 
    public function getEngineer()
    {
        return $this->engineer;
    }

    /**
     * Set the value of engineer
     */ 
    public function setEngineer($engineer)
    {    
        $this->engineer = $engineer;
    }

    public function jsonSerialize() {
        return [
            "id"            => $this->id,
            "title"         => $this->title,
            "description"   => $this->description,
            "createdAt"     => $this->createdAt,
            "closedAt"      => $this->closed,
            "recorder"      => $this->recorder,
            "engineer"      => $this->engineer,
        ];
    }
    
}