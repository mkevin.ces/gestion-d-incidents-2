<?php

namespace BugApp\Models;

use BugApp\Services\Manager;
use BugApp\Models\Engineer;
use BugApp\Models\Recorder;

class UserManager extends Manager
{
    

    public function findByEmail($email)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM user WHERE email = :email');
        $sth->bindParam(':email', $email);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        if($result != null) {
            switch($result["type"]) {
                case 'recorder':
                    //recorder
                    $user = new Recorder();
                    $user->setId($result["id"]);
                    $user->setNom($result["nom"]);
                    $user->setEmail($result["email"]);
                    $user->setPassword($result["password"]);
                    return $user;
                    break;
                case 'engineer':
                    //ingenieur
                    $user = new Engineer();
                    $user->setId($result["id"]);
                    $user->setNom($result["nom"]);
                    $user->setEmail($result["email"]);
                    $user->setPassword($result["password"]);
                    return $user;
                    break;
            }
        }else{
            return null;
        }
        // Retour
        
    }

    public function check($user, $mdp) {
        return $user->getPassword() == $mdp;
    }







}
