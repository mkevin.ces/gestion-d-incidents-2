<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{
    public function findIdEngineer($id_user){
        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT id FROM engineer WHERE engineer.user_id = :id_user');
        $sth->bindParam(':id_user', $id_user, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['id'];
    }

    public function findIdRecorder($id_user){

        $dbh = static::connectDb();

        $sth = $dbh->prepare('SELECT id FROM recorder WHERE recorder.user_id = :id_user');
        $sth->bindParam(':id_user', $id_user, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        return $result['id'];
    }

    public function findUserFromType($id_type, $type) {

        // Connexion à la BDD
        $dbh = static::connectDb();

        $requete = "";
        $user = null;

        switch($type) {
            case 'recorder':
                $user = new Recorder();
                $requete = "SELECT u.id, nom, email 
                            FROM user u 
                            INNER JOIN recorder r ON u.id = r.user_id 
                            WHERE r.id = :id";
                break;
            case 'engineer':
                $user = new Engineer();
                $requete = "SELECT u.id, nom, email
                            FROM user u 
                            INNER JOIN engineer e ON u.id = e.user_id 
                            WHERE e.id = :id";
                break;
            default:
        }

        $sth = $dbh->prepare($requete);
        $sth->bindParam(':id', $id_type, \PDO::PARAM_INT);
        $sth->execute();

        $resultat = $sth->fetch(\PDO::FETCH_ASSOC);

        if(empty($resultat)) {
            return null;
        }

        $user->setId($resultat['id']);
        $user->setNom($resultat['nom']);
        $user->setEmail($resultat['email']);

        return $user;
        
    }

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);

        
        $bug->setRecorder($this->findUserFromType($result["recorder_id"], 'recorder'));
        $bug->setEngineer($this->findUserFromType($result["engineer_id"], 'engineer'));

        // Retour
        return $bug;
    }

    public function findAll()
    {

        // Récupération de tous les incidents en BDD
                // Connexion à la BDD
                $bugTab = [];
                $dbh = static::connectDb();

                // Requête
                $sth = $dbh->prepare('SELECT B.id,B.title,B.description,B.createdAt,B.closed,U.nom Recorder,U_2.nom Engineer
                FROM bug B 
                left join recorder R on B.recorder_id=R.id 
                left join user U on R.user_id=U.id
                left join engineer E on B.engineer_id=E.id 
                left join user U_2 on E.user_id=U_2.id');
                $sth->execute();
                
        
                while($result = $sth->fetch(\PDO::FETCH_ASSOC)){
                    // Instanciation d'un bug
                    $bug = new Bug();
                    $bug->setId($result["id"]);
                    $bug->setTitle($result["title"]);
                    $bug->setDescription($result["description"]);
                    $bug->setCreatedAt($result["createdAt"]);
                    $bug->setClosedAt($result["closed"]);
                    $bug->setRecorder($result["Recorder"]);
                    $bug->setEngineer($result["Engineer"]);

                    array_push($bugTab,$bug);
                }

                return $bugTab;

    }
    public function findAllEngineerAssigntome($id_user,$string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,U.nom Nom_recorder, B.createdAt,B.closed,U_2.nom Nom_engineer,engineer_id
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where U_2.id = ? and B.title like "%'.$string.'%"     
        ORDER BY createdAt desc');
        $sth->execute([$id_user]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }
    public function findAllEngineerNotClosed($string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,U.nom Nom_recorder, B.createdAt,B.closed,U_2.nom Nom_engineer,engineer_id
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where closed is null and B.title like "%'.$string.'%"     
        ORDER BY createdAt desc');
        $sth->execute();
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }
    public function findAllEngineerLesdeux($id_user,$string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,B.createdAt,B.closed,U_2.nom Nom_engineer,U.nom Nom_recorder
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where closed is null and U_2.id = ? and B.title like "%'.$string.'%"    
        ORDER BY createdAt desc');
        $sth->execute([$id_user]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function findAllsearch($string)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT B.id,B.title,B.description,U.nom Nom_recorder, B.createdAt,B.closed,U_2.nom Nom_engineer,engineer_id
        FROM bug B 
        left join recorder R on B.recorder_id=R.id 
        left join user U on R.user_id=U.id
        left join engineer E on B.engineer_id=E.id 
        left join user U_2 on E.user_id=U_2.id
        where B.title like "%'.$string.'%"     
        ORDER BY createdAt desc');
        $sth->execute([]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        $bug->setRecorder($result["Nom_recorder"]);
        $bug->setEngineer($result["Nom_engineer"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function findAllRecorder($id_user)
    {

        // Récupération de tous les incidents en BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT bug.id, title, description,createdAt,closed
        FROM bug, recorder
        WHERE bug.recorder_id = recorder.id AND recorder.user_id = ?
        ');
        $sth->execute([$id_user]);
        $bugs=[];
        while($result = $sth->fetch(\PDO::FETCH_ASSOC)){;

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);
        array_push($bugs,$bug);
        };
         // Retour
         return $bugs;
    }

    public function add(Bug $bug,Recorder $user){

        // Ajout d'un incident en BDD
        $bdd =static::connectDb();

        $req_insert=$bdd->prepare('insert into bug (title, description, createdAt,recorder_id)values (?,?,?,?)');
        $req_insert->execute([$bug->getTitle(), $bug->getDescription(), $bug->getCreatedAt()->format("Y-m-d H:i:s"),$this->findIdRecorder($user->getId())]);
    }

    public function updateAssigner(Bug $bug, Engineer $engineer){

        $bdd =static::connectDb();

        $bug_id = $bug->getId();
        $engineer_id = $this->findIdEngineer($engineer->getId());

        $req_update = $bdd->prepare('   UPDATE bug
                                        SET engineer_id = :engineer_id
                                        WHERE bug.id = :bug_id');
        $req_update->bindParam(':engineer_id', $engineer_id, \PDO::PARAM_INT);
        $req_update->bindParam(':bug_id', $bug_id, \PDO::PARAM_INT);
        $req_update->execute();

    }

    public function updateCloture(Bug $bug){

        $bdd =static::connectDb();

        $bug_id = $bug->getId();
        $today = date('Y-m-d H:i:s');

        $req_update = $bdd->prepare('   UPDATE bug
                                        SET closed = :today
                                        WHERE bug.id = :bug_id');
        $req_update->bindParam(':today', $today, \PDO::PARAM_STR);
        $req_update->bindParam(':bug_id', $bug_id, \PDO::PARAM_INT);
        $req_update->execute();

        return $today;

    }







}
