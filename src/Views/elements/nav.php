<body>
<nav class="blue-grey lighten-2" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="#" class="brand-logo">Logo</a>
            <ul class="right hide-on-med-and-down">

                <li>
                    <?php
                        if(isset($_SESSION['user'])) {
                            echo ($_SESSION['user'])->getNom();
                            //href="logout" nous renvoie vers la "page" de logout et detruit la session courante
                            echo    '<li>
                                        <a href="logout">Logout</a>
                                    </li>';
                        }
                    ?> 
                </li>


                <li><a href="#"><i class="material-icons">account_box</i></a></li>
            </ul>
            <ul id="nav-mobile" class="sidenav">
                <?=(isset($_SESSION['user'])) ? ($_SESSION['user'])->getNom() : "personne :("?>
                <li><a href="#">Logout</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>