<?php

  $bugs = $parameters['bugs'];
  $connected_engineer = $parameters['connectedUser'];

  include('../src/Views/elements/header.php');
  include('../src/Views/elements/nav.php');

?>      

<div class="container">
    </br></br>
    <h3 class="global-title">Liste des incidents</h3>
    <p>
        <label>
          <input type="checkbox" id="notclosed"/>
          <span>Afficher uniquement les incidents non clôturés</span>
        </label>
      </p>
      <!-- Fin création d'un checkbox -->
      <p>
        <label>
          <input type="checkbox" id="assigntome"/>
          <span>Afficher uniquement les incidents que je me suis assignés</span>
        </label>
      </p>
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">search</i>
          <input id="research" type="text" class="validate" placeholder="Saisir le sujet...">
        </div>
    <table class="striped responsive-table">
      <thead class="grey lighten-2">
      <tr>
                        <th width="14%">ID</th>
                        <th width="14%">Sujet</th>
                        <th width="14%">Date</th>
                        <th width="14%">Utilisateur</th>
                        <th width="16%"></th>
                        <th width="14%">Ingénieur</th>
                        <th width="14%">Clotûre</th>        
                    </tr>
      </thead>
      <tbody id="donnees">
          <?php foreach($bugs as $value){
                if ($value->getClosedAt() != ""){
                  $date = $value->getClosedAt()->format('Y-m-d');
                } else if ($connected_engineer->getNom() == $value->getEngineer()) {
                  $date = '<button class="waves-effect waves-light btn-small" value="'.$value->getId().'"  id="cloture_bug">Clôturer</button>';
                } else if($value->getClosedAt() == "") {
                  $date = "En cours";
                }
                if($value->getEngineer() == ""){
                  $engineer = '<button class="waves-effect waves-light btn-small" value="'.$value->getId().'" id="assigne_bug">Assigner</button>';
                }else{
                  $engineer = $value->getEngineer();
                }
                echo '<tr>
                    <td>'.$value->getId().'</td>
                    <td>'.$value->getTitle().'</td>
                    <td>'.$value->getCreatedAt()->format('Y-m-d').'</td>
                    <td>'.$value->getRecorder().'</td>
                    <td><a class="waves-effect waves-light btn-small" href="bug/show/'.$value->getId().'"><i class="material-icons left">subject</i>Afficher</a></td>
                    <td>'.$engineer.'</td>
                    <td>'.$date.'</td>
                </tr>'; 
          }?>
      </tbody>
    </table>
  </div>
  </br></br></br>

<?php 
  include('../src/Views/elements/footer.php');
?>