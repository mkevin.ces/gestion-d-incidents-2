<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];

    include('../src/Views/elements/header.php');
    include('../src/Views/elements/nav.php');

?>
<div class="container">
        </br></br>
        <a class="link-top" href="http://localhost:8000/bug"><i class="chev small material-icons">chevron_left</i> Retour à la
            liste</a>
        <h3 class="global-title">Fiche d'escriptive incidents</h3>
        </br>
        <div class="row">
            <div class="row">
                <div class="col s6">
                    <p>Nom de l'incidents : <?php echo $bug->getTitle();?></p>
                </div>
                <div class="col s6">
                    <p>Date d'observation : <?php echo $bug->getCreatedAt()->format('d-m-Y');?></p>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <p> <?php echo $bug->getDescription();?></p>
                </div>
            </div>
        </div>
    </div>
    </br></br></br>

<?php 

include('../src/Views/elements/footer.php');

?>