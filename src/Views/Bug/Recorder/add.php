<?php 

include('../src/Views/elements/header.php');
include('../src/Views/elements/nav.php');

?>
<div class="container">
        </br></br>
        <a class="link-top" href="http://localhost:8000/bug"><i class="chev small material-icons">chevron_left</i> Retour à la
            liste</a>
        <h3 class="global-title">Rapport d'incidents</h3>
        </br></br>
        <div class="row">
            <form class="col s12" method="POST" >
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder="Nom de l'incidents" id="first_name" type="text" class="validate" name="nom">
                        <label for="first_name">Nom de l'incidents</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" type="date" class="validate" name="date">
                        <label for="last_name">Date d'observation</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="textarea1" class="materialize-textarea" name="desc"></textarea>
                        <label for="textarea1">Description de l'incident</label>
                    </div>
                </div>
                <button class="btn waves-effect waves-light" type="submit" name="action">Envoyer
                    <i class="material-icons right">send</i>
                </button>
            </form>
        </div>
    </div>
    </br></br></br>

<?php 
    include('../src/Views/elements/footer.php');
?>