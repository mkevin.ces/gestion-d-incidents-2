<?php

$bugs = $parameters['bugs'];

include('../src/Views/elements/header.php');
include('../src/Views/elements/nav.php');

?>      

<div class="container">
    </br></br>
    <h3 class="global-title">Liste des incidents</h3>
    <a class="btn-floating btn-medium waves-effect waves-light blue darken-2" href="/bug/add"><i
        class="material-icons">add</i></a> Rapporter un incidents</br></br>
    <table class="striped responsive-table">
      <thead class="grey lighten-2">
        <tr>
          <th>Id</th>
          <th>Sujet</th>
          <th>Date</th>
          <th>Clôture</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
          <?php foreach($bugs as $value){
              
                if($value->getClosedAt() == ""){
                    $date = "Pas de date";
                }else{
                    $date = $value->getClosedAt()->format('Y-m-d');
                }
                echo '<tr>
                    <td>'.$value->getId().'</td>
                    <td>'.$value->getTitle().'</td>
                    <td>'.$value->getCreatedAt()->format('Y-m-d').'</td>
                    <td>'.$date.'</td>
                    <td><a class="waves-effect waves-light btn-small" href="bug/show/'.$value->getId().'"><i class="material-icons left">subject</i>Afficher</a></td>
                </tr>'; 
          }?>
      </tbody>
    </table>
  </div>
  </br></br></br>

<?php 

include('../src/Views/elements/footer.php');

?>