<?php
  if(!empty($parameters)) {
    $error = $parameters['error'];
  } else {
    $error = null;
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Starter Template - Materialize</title>

  <!-- CSS  -->
    <?php 
        include('../src/Views/elements/header.php');
        include('../src/Views/elements/nav.php');
    ?>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">Login</h3>
      <div class="row center">
        <div class="col s3">
        </div>
        <div class="col s6">

  <form class="col s12" method="post">
    <div class="row">
      <div class="input-field col s12">
        <i class="material-icons prefix">account_circle</i>
        <input id="icon_prefix" name="user" type="text" class="validate">
        <label for="icon_prefix">Username</label>
      </div>
      <div class="input-field col s12">
        <i class="material-icons prefix">lock</i>
        <input id="icon_telephone" type="password" name="pwd" class="validate">
        <label for="icon_telephone">Password</label>
      </div>
    </div>
    <?php 
      if($error != null) {
        echo $error;
      }
    ?>
    <div class="row">
      <button class="btn waves-effect waves-light" type="submit" name="submit">Log in
      <i class="material-icons right">send</i>
      </button>
    </div>
  </form>
        </div>
        <div class="col s3">
        </div>
      </div>
      <div class="row center">

      </div>
      <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">


    </div>
    <br><br>
  </div>
    <?php include('../src/Views/elements/footer.php'); ?>
  </body>
</html>